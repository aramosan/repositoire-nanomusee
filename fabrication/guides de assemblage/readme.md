# Guides d'Assemblage

Pour chaque Module

* Description de l'assemblage de toute pièce:
    * planches <= boulons => corniers
    * Trappes magnétiques
    * Accroche pour casquette/brochure
    * Dispositifs particuliers
        * Écrans
        * Planches en Plexiglass coulissantes
        * lampadaires



L'objectif ici n'est pas de produire une multitude de documents détaillant chaque petit aspect de l'assemblage pour chaque module, ce qui n'est pas nécessaire. En effet, il serait possible de présenter un document uniquement pour illustrer, en visualisation éclatée, l'utilisation des cornières/planches en fonction : de la quantité de coins (ce qui n'est pas toujours évident) ou de leur position dans le module (les cornières/jonctions servant de pieds sont différentes).

Ainsi, chaque module possède ses particularités, et il serait judicieux de rédiger un document qui les aborde, avec chaque page dédiée à un module spécifique. Par exemple, pour l'alguier, il faudrait specifier le montage des 2 lampadaires carrés ainsi que celui des plaques coulissantes. Pour les modules plus simples, comme l'observatoire, un rendu 3D avec une légendage avec des lignes de connexion suffit.

# Planches - découpage

Fichiers .ai et .pdf. À ouvrir avec les logiciels _Adobe Illustrator_ (proprietaire) ou importer sur _Inkscape_ (openSource). Version 2 seulement.

### Référence Peinture

* Marque: UNIKALO
* Type: O2LAK INFINITY MAT VELOUTÉ
* Teinte: Chromatic CH1 0922
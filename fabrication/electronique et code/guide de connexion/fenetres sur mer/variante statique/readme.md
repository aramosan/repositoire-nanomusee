
# Liste de Matériaux (Bill of Materials)

<br>

> Quantités pour 4 fenêtres. Trouvez-le également dans le fichier _liste de matériaux.xlsx_

> Se référer au schéma de connexion pour comprendre la l'ID de Matériel

<br>

## Variante statique 


ID | Matériel | Quantité | Commentaire
-- | -------- | -------- | -----------
**Alimentation** |
0 | Multiprise avec interrupteur | 1 | Allumage général direct avec l'interrupteur
1 | Câble alimentation (2 canaux) | 1 |
**Éclairage**
2 | Douille de lampe E27 | 4 | Max 60W
3 | Ampoule LED E27  | 4 | Classe énergetique A ou B. Temperature Froide: 2700K



# Liste de Matériaux (Bill of Materials)
### Fenêtres sur Mer


> Quantités pour 4 fenêtres. Trouvez-le également dans le fichier _liste de matériaux.xlsx_

> Se référer au schéma de connexion pour comprendre la l'ID de Matériel

<br>

## Variante interactive 


<br>

ID | Matériel | Quantité | Commentaire
-- | -------- | -------- | -----------
**Alimentation** |
0 | Multiprise avec interrupteur | 1 | Allumage général direct avec l'interrupteur
1 | Transformateur d'Alimentation 12V 5A | 1 | 
2 | Câble répartiteur d'alimentation (barrel jack 2.1mm) | 1 | 1 entrée femelle >> 4 sorties mâle
3 | Câble alimentation (2 canaux) | 1 |
4 | Prise d'alimentation Barrel Jack 2,1mm Femelle | 4 | Pour Boîtier
5 | Connecteurs barrel Jack 2.1mm de Prise D'alimentation mâle | 4 |
**Boîtier Fenêtre**
6 | Boîtier de protection | 4 | taille minimale:  10 x 6.8 x 5cm
7 | MicroControleur Arduino Nano | 4
**Éclairage**
8 | LED 10W 12V - Puce Blanche/Froide SMD COB | 4 | forme: carré de 2x2cm aprox
9 | Câble (2 canaux) | 1 
10 | Connecteur JST SM (2 canaux) | 8
**Capteurs**
11 | Capteurs Proximité TOF200C VL53L0X | 4
12 | Câble (4 canaux) | 1
13 | Connecteur JST SM (4 canaux) | 8
**MicroÉlectronique**
14 | Resistances de 1/4W | 1K (x2) , 2.2K (x2)
15 | TIP122: transisteur de puissance | 4
16 | LED 5mm, 3V ~20mA | 4

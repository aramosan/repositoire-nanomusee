#include <Wire.h>
#include <VL53L0X.h>
#ifdef __AVR__
#include <avr/power.h>  // Required for 16 MHz Adafruit Trinket
#endif



// --------- Capteur Time Of Flight : TOF050C-VL6180X
VL53L0X sensor;


// Uncomment ONE of these two lines to get
// - higher speed at the cost of lower accuracy OR
// - higher accuracy at the cost of lower speed
//#define HIGH_SPEED
#define HIGH_ACCURACY

int triggerDistance = 1000;  // millimetres;

// ----------

int refreshRate = 500;
bool lightsOn = false;


// ------- LED 10W

#define LED_PIN 9

float lightIntensity = 0;
float fadeSpeed = 0.03;
int waitAfterTurningOn = 20000;  // REAL: 20000 milliseconds, TEST: 500



void setup() {
  Serial.begin(9600);
  Wire.begin();

  int errorCode = 0;

  sensor.setTimeout(500);

  if (!sensor.init()) {
    Serial.println("Failed to detect and initialize sensor!");
    errorCode = 1;
  }

#if defined HIGH_SPEED
  // reduce timing budget to 20 ms (default is about 33 ms)
  sensor.setMeasurementTimingBudget(20000);
#elif defined HIGH_ACCURACY
  // increase timing budget to 200 ms
  sensor.setMeasurementTimingBudget(200000);
#endif



  init(errorCode);

  // Serial.println("====== START LOOP");
}

void init(int errorCode) {

  // LE LEDS CLIGNOTENT UN CODE ERREUR

  if (errorCode == 0) {
    // AUCUN ERREUR
    // BLINK LED 3 trois fois
    blink(3);
  } else {
    // AU CAS DE ERREUR, ATTENDRE LE BLINKING DES TOUS LE LEDs POUR APRES BLINKER LE MESSAGE D'ERREUR
    delay(2000);
  }


  //-----
  // ERREURS A BLINKER : EN CAS D'ERREUR LE LED DEVRA BLINKER SON CODE UN FOIS FINIE L'INITIALIZATION PRECEDENTE)

  if (errorCode == 1) {
    // SENSOR NOT INITIALIZED 
    blink(1);
  }
}

void loop() {


  // ----- DISTANCE SENSOR
  int currentDistance = sensor.readRangeSingleMillimeters();
  // Serial.print(distance);

  if (sensor.timeoutOccurred()) {
    Serial.print("TIMEOUT");
  }


  if (!lightsOn) {
    if (checkPresence(currentDistance)) {
      fadeIn();  // TIME TAKEN = fadeIn + waitAfterTurningOn
    }
  } else {
    // SI LA PERSONNE EST PARTIE, fadeOut()
    // (ELSE) SI LA PERSONNE EST TOUJOURS LÀ, ATTENDRE 5 SEC AVANT DE REVENIR ICI

    //Serial.println("END OF LIGHT ON :: CHECKING PRESENCE");

    if (!checkPresence(currentDistance)) {
      //Serial.println("FADE OUT :: PERSONNE N'EST LÀ");
      fadeOut();
    } else {
      //Serial.println("ATTENDRE 5 SECS :: QUELQU'UN EST TOUJOURS LÀ");
      delay(5000);
    }
  }

  delay(refreshRate);
}


bool checkPresence(int distance) {

  //Serial.print("Distance => ");
  //Serial.println(distance);


  // BRUIT: QUAND LE CAPTEUR NE CAPTE PERSONNE (IL N'Y A RIEN DEVANT LE CAPTEUR), MAIS AUSSI QUAND IL Y A
  // QUELQUE CHOSE SUR L'ANGLE PERIPHERIQUE, LES VALEURS BASCULENT ENTRE 8190 (LE VIDE, L'INFINI, LA SINGULARITE EXISTENCIELLE) ET
  // DES VALEURS PAR DESSOUS 200

  // SI MOINS DE 200 (20 CM), FAIT COMME SI IL'N Y A PERSONNE
  if (distance < 200) {
    // Serial.println("- BRUIT LOW : PRESENCE FALSE");
    return false;
  }

  if (distance < triggerDistance) {
    //Serial.println("- PRESENCE : YES : BELOW 1000");
    return true;
  } else {
    // Serial.println("- PRESENCE : NO  : ABOVE 1000");
    return false;
  }
}




void fadeIn() {

  // Serial.println("-- FADE IN : START");

  lightIntensity = 0;

  while (lightIntensity < 1) {

    analogWrite(LED_PIN, int(lightIntensity * 255));

    lightIntensity += fadeSpeed;

    delay(30);
  }

  lightIntensity = 1;
  analogWrite(LED_PIN, int(lightIntensity * 255));

  lightsOn = true;

  // Serial.println("-- FADE IN : END");

  delay(waitAfterTurningOn);
}

void fadeOut() {
  // Serial.println("-- FADE OUT : START");

  lightIntensity = 1;

  while (lightIntensity >= 0) {

    analogWrite(LED_PIN, int(lightIntensity * 255));

    lightIntensity -= fadeSpeed;

    delay(60);
  }

  lightIntensity = 0;
  analogWrite(LED_PIN, int(lightIntensity));

  lightsOn = false;

  // Serial.println("-- FADE OUT : END");
}

void blink(int fois) {

  for (int i = 0; i < fois; i++) {
    analogWrite(LED_PIN, 0);
    delay(340);
    analogWrite(LED_PIN, 127);
    delay(340);
  }
  analogWrite(LED_PIN, 0);

  // ATTENDRE UN PEU PLUS POUR NE PAS COLLER LES SEQUENCES DE BLINKS
  delay(1000);
}

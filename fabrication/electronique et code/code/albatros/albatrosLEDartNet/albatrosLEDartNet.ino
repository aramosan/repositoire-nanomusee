// Please include ArtnetWiFi.h to use Artnet on the platform
// which can use both WiFi and Ethernet
#include <ArtnetWiFi.h>
#include <Adafruit_NeoPixel.h>
// this is also valid for other platforms which can use only WiFi
// #include <Artnet.h>

// WiFi stuff
const char* ssid = "Nano Musée";
const char* pwd = "Nanomusee2023";

// LED Strip
const int numLeds = 288; // Change if your setup has more or less LED's
const int numberOfChannels = numLeds * 4; // Total number of DMX channels you want to receive (1 led = 3 channels)
#define DATA_PIN 4 //The data pin that the WS2812 strips are connected to.
#define BRIGHTNESS 255 // Set BRIGHTNESS to about 1/5 (max = 255)
Adafruit_NeoPixel strip(numLeds, DATA_PIN, NEO_GRBW + NEO_KHZ800);

// Artnet
ArtnetWiFiReceiver artnet;
uint8_t universe1 = 1;  // 0 - 15
uint8_t universe2 = 2;  // 0 - 15
uint8_t universe3 = 3;  // 0 - 15

bool serialPrint = false;

void callback(const uint8_t* data, const uint16_t size) {
    // you can also use pre-defined callbacks
}

void writeLEDs(int led_offset, const uint8_t* data, const uint16_t size, int universe) {
    // if Artnet packet comes to this universe, this function (lambda) is called
    if (serialPrint)
    {
      Serial.print("lambda : artnet data (universe : ");
      Serial.print(universe);
      Serial.print(", size = ");
      Serial.print(size);
      Serial.print(") :");
      for (size_t i = 0; i < size; ++i) {
        Serial.print(data[i]);
        Serial.print(",");
      }
    }

    int led_index = 0;
    for (size_t i = 0; i < 512; i=i+4) {
        const uint8_t R = data[i];
        const uint8_t G = data[i+1];
        const uint8_t B = data[i+2];
        const uint8_t W = data[i+3];
        strip.setPixelColor(led_index+led_offset, strip.Color(0, 0, 0, G));
        //strip.setPixelColor(led_index+led_offset, strip.Color(255, 0, 0,0));
        led_index++;            
        if( (led_index+led_offset) >= numLeds)
        {
        Serial.print("ERROR,  (led_index+led_offset) >= numLeds");
              Serial.println();      
        Serial.print(led_index);
              Serial.println();      
        Serial.print(led_offset);
              Serial.println();      
        Serial.print(numLeds);
              Serial.println();      
        }
    }  

    strip.show();  

    if (serialPrint)
    {
      Serial.print("led_index = ");
      Serial.print(led_index);
      Serial.println();      
    }
}

void setup() {
    Serial.begin(115200);

    // WiFi stuff
    WiFi.begin(ssid, pwd);
    //WiFi.config(ip, gateway, subnet);
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }
    Serial.print("WiFi connected, IP = ");
    Serial.println(WiFi.localIP());

    artnet.begin();
    // artnet.subscribe_net(0);     // optionally you can change
    // artnet.subscribe_subnet(0);  // optionally you can change

    // if Artnet packet comes to this universe, this function (lambda) is called
    artnet.subscribe(universe1, [&](const uint8_t* data, const uint16_t size) {
      writeLEDs(0,data,size,universe1);      
    });
    artnet.subscribe(universe2, [&](const uint8_t* data, const uint16_t size) {
      writeLEDs(128,data,size,universe2);
    });
    artnet.subscribe(universe3, [&](const uint8_t* data, const uint16_t size) {
      writeLEDs(256,data,size,universe3);
    });   
    strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
    strip.setBrightness(BRIGHTNESS);
    strip.show();            // Turn OFF all pixels ASAP

    // you can also use pre-defined callbacks
    //artnet.subscribe(universe2, callback);
}

void loop() {
    artnet.parse();  // check if artnet packet has come and execute callback
}

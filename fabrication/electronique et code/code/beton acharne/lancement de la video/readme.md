# Guide d' configuration de Bétón Acharné

Guide pour lancer une vidéo automatiquement lors de démarrage du système. Utile pour les cartes xPI o similaires (RaspberryPi, BananaPi, OrangePi, LibreComputer Board, Asus TinkerBoard, etc.) Pour le software, il sert pour des système d’exploitation tels que le RaspberryPi OS, Raspbian, Ubuntu Mate, etc.




### Ce qui est utilisé sur Béton Acharné:

> Hardware:

* Banana Pi M3 v1.2
* Alimentation via microUSB (sur le côté court), à 5V 2A minimum.
* Sortie vidéo: HDMI type A (le normal)
* Sortie USB: 2, de Type A (le normal). Attention, on ne peut pas brancher le clavier, la souris ET une clé USB (pour transférer les fichiers) en même temps. Il faut se munir de quelque chose comme un clavierSouris bluetooth avec un seul connecteur, un hub USB, ou tout faire avec la souris.

> Système d'exploitation:
* Ubuntu Mate 16.04.5 LTS (xenial)
* user: pi
* password: pi      (si requis)


<br>



## Les Pas

Il faut créer 3 fichiers:

1) La vidéo à reproduire, en format .mp4
2) le script bash pour ouvrir l’application de reproduction correcte, et en pleine écran.
3) une script ou configuration pour exécuter le script bash automatiquement lors du démarrage.

et il faut installer l’application MPV Video Player, via la ligne de commande: <code>sudo apt-get install mpv</code>

Les fichiers pour les étapes 2 et 3 sont fournis, en suppossant que l'utilisateur est <code>pi</code>. Vous pouvez alors simplement copier-coller les fichiers dans le dossier mentionnés. Si l'utilisateur n'est pas <code>pi</code> il faudra le remplacer dans les scripts avec le correct, comme explique ci-dessous.


### 1 : Vidéo

Copier la vidéo dans un dossier de predilection. Nous recommandons: <code>home/[utilisateur]/betonAcharne/video.mp4</code>
Ici, <code>[utilisateur]</code> fait reference au nom de l’utilisateur définit au moment de l’installation du système d’exploitation., Par default, c’est <code>pi</code>.
Une fois installée l’application MPV, essayer d’ouvrir la vidéo afin de tester que le format/codec peut bien se reproduire.

<br>

### 2 : Script Bash
Dans le même dossier où se trouve la vidéo, créer un nouveau fichier appelé <code>lancerVideo.sh</code> .  Cela peut se faire avec n’importe que éditeur de texte simple, ou même sur la ligne de commande en se plaçant dans le dossier et en tapotant <code>touch lancerVideo.sh</code>

#### Editer le script: 
Voici les commandes à insérer à l’intérieur du script:

<code>#!/bin/bash</code>

<code>sleep 10</code>

<code>mpv --loop=inf --fullscreen home/[utilisateur]/betonAcharne/video.mp4 &</code>

Une fois écrites ces 3 lignes, sauvegarder et sortir.

</br>

### 3 : Démarrage automatique

Pour que le fichier <code>lancerVideo.sh</code> soit lancé au démarrage du système, il faut l'inclure en tant qu’Application de démarrage. 2 manières de le faire:

#### 1 : Via Command Line
Créer et éditer le fichier <code>~/.config/autostart/betonAcharneStart.desktop</code> avec les données ci-dessous.

( <code>.config</code> est un fichier caché, peut-être il ne s'affiche pas)
(Tout fichier situé dans ce dossier "autostart" s'exécute automatiquement, sauf si le flag <code>X-MATE-Autostart-enabled = false</code>)

#### 2 : Via l'interface graphique
Faire pareil sur System -> Control Center -> Personal -> StartUp Applications

> DONNÉES:

<code>Name = Demarrer Video</code>

<code>Command = bash /home/[utilisateur]/betonAcharne/lancerVideo.sh</code>

<code>Comment = demarrerVideo.sh</code>



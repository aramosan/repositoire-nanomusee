
Licence Personnalisée Non Commerciale

Titre: repositoire-nanomusee
Auteur	: Cellule Science et Société, La Rochelle Université

Termes et Conditions:
Ce dépôt est dédié au domaine public sous la Déclaration de Domaine Public Creative Commons CC0 1.0 Universelle avec les conditions supplémentaires suivantes :

1. Vous pouvez utiliser le contenu de ce dépôt à des fins non commerciales sans exigence d'attribution.
2. L'utilisation commerciale est interdite sans le consentement écrit préalable de l'auteur.

Pour consulter une copie de cette dédicace, visitez le site https://creativecommons.org/publicdomain/zero/1.0/

--

English version:

Custom Non-Commercial License

Title: repositoire-nanomusee
Author: Cellule Science et Société, La Rochelle Université

Terms:
This repository is dedicated to the public domain under the Creative Commons CC0 1.0 Universal Public Domain Dedication with the following additional terms:
1. You may use the contents of this repository for non-commercial purposes without the need for attribution.
2. Commercial use is not permitted without prior written consent from the author.

To view a copy of this dedication, visit https://creativecommons.org/publicdomain/zero/1.0/


# <a id="top"></a>Bandes Dessinées


Conception Graphique: [Junie Briffaz](https://www.instagram.com/junie_briffaz/)

Ce travail à bénéficié d'une aide du Plan France 2030, référence ANR-21-EXES-0010

![logos partenaires](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/bd/logos_partenaires.png)

</br>

---

## Guide

Trouvez dans ce dossier du repositoire les BD en format écran ( « *_écran* », allongé en verticale) et en format d'impression ( « *_print* », à plier).

### Versiones à imprimer: Instructions de pliage
Les versiones *_print* ont été conçues pour un format A4, à plier selon la guide ci-dessous.

![Guide de pliage](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/bd/instructions_de_pliage_ecran.png)


---


## Albatros

[![albatros thumbnail](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/bd/albatros_bd_thumb.png)](#top)

## L'Alguier
[![alguieer thumbnail](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/bd/alguier_bd_thumb.jpg)](#top)


## Archéologie du Silence
[![archeologie thumbnail](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/bd/archeologieDuSilence_bd_thumb.jpg)](#top)


## Béton Acharné
[![beton thumbnail](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/bd/betonAcharne_bd_thumb.jpg)](#top)


## L'Observatoire
[![observatoire thumbnail](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/bd/observatoire_bd_thumb.png)](#top)

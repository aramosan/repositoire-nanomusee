<details open>
<summary>Modules</summary>

* [L'Alguier](#alguier)
* [Béton Acharné](#beton)
* [Albatros](#albatros)
* [Archéologie du Silence](#archeologie)
* [L'Observatoire](#observatoire)
* [Fenêtres sur Mer](#fenetres)

</details> 

</br>

---

</br>



<a id="alguier"></a>![Header Alguier](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/header_1_alguier.png)

par Gwen Le Gac et Ingrid Arnaudin.

## Chapô

L’Alguier, corpus d’œuvres imaginé pour le NANOmusée par l'artiste Gwen Le Gac, se compose de vingt-et-un dessins réalisés au feutre avec une technique aquarelle. La reproduction de ces dessins par sérigraphie sur plexiglas peut évoquer le cabinet de curiosité, ou encore les lames de microscope d’un laboratoire de recherche scientifique.

Inspiré d’un alguier du XIXe siècle ainsi que par sa résidence d’artiste, la série de dessins et la lecture des légendes par l’artiste célèbrent la diversité des espèces d’algues. L’Alguier rend justice à leur beauté, en nous invitant à questionner nos préjugés sur ces végétaux aux nombreux bienfaits.

> D’APRÈS : la rencontre entre l’artiste Gwen Le Gac et l’enseignante-chercheuse Ingrid Arnaudin au laboratoire LIENSs (La Rochelle Université, CNRS)

> SUJET DE RECHERCHE : Valorisation des molécules produites par les algues du littoral charentais.

> LABORATOIRE D'ACCUEIL : Littoral ENvironnement et Sociétés (LIENSs) - UMR 7266 (La Rochelle Université, CNRS)

> Remerciements : Ingrid Arnaudin (La Rochelle Université), Hugo Groult (CNRS), Jonathan Izambart (ADERA), Hélène Jouannet et Tanguy Gauvin (Algorythme)

## Genèse de l’œuvre

L’histoire de L’Alguier commence un matin comme les autres, un simple jour d’été sur la plage de Kersidan dans le Finistère breton. Notre artiste, Gwen, flâne en lisant le journal local lorsqu’un petit article attire son attention : on y parle d’un herbier présentant une collection d’algues - autrement dit un « alguier -, réalisé au XIXème siècle et exposé au marinarium de Concarneau. Le marinarium n’est pas très loin : poussée par la curiosité, Gwen décide d’aller à sa rencontre, mais elle apprendra que ce livre précieux a été transporté au Muséum d’Histoire Naturelle de Paris pour y être numérisé et protégé.

De retour chez elle à Paris, quand elle parvient enfin à accéder à l’alguier numérisé, elle se sent touchée, envoûtée, profondément inspirée par ses images, par la poésie involontaire de ses légendes littéraires et par son histoire : celle d’un livre en 3 volumes publié en 1852 par deux frères pharmaciens de Brest, Pierre et Hippolyte Crouan. Il répertorie non moins de 404 échantillons d’algues d’espèces différentes.

Peut-être croyez-vous que Gwen s’est mise le soir même à esquisser la multitude d’images que lui avait inspirée cette découverte, mais les œuvres d’art prennent parfois du temps pour naître. Toutes ses idées, son émerveillement et toute cette inspiration sont ainsi restés là, dans un coin de sa tête, à s’étoffer petit à petit…

Plusieurs années passent. Un jour, La Rochelle Université lui propose une résidence d’artiste au laboratoire LIENSs, spécialisé sur les problématiques liées au littoral. Gwen a ainsi la chance d’y rencontrer Ingrid Arnaudin, une enseignante-chercheuse, ainsi que ses collègues dont les travaux se portent sur les algues. 

C’est au moment de sa résidence d’artiste à La Rochelle Université pour le projet du NANOmusée que Gwen rencontre une enseignante-chercheuse, Ingrid Arnaudin, au laboratoire LIENSs. Ses collègues et elles lui ouvrent la porte de leur laboratoire, le LIENSs, et lui parlent de leurs travaux ainsi que des nombreuses qualités que possèdent les algues. Elle suit Ingrid sur le terrain jusqu’à la ferme Algorythme, ou ses fondateurs Hélène et Tanguy cultivent diverses espèces d’algues. Lors de sa résidence, Gwen est curieuse de découvrir leurs recherches, que les algues sont utilisées dans l’industrie de l’agro-alimentaire, de la pharmaceutique, dans la recherche en cancérologie, et qu’elles pourraient apporter de nombreux bienfaits et solutions à l’avenir.

Elle relève les couleurs de ces différentes espèces d’algues : « On les imagine souvent vertes ou noires, séchées et pourrissantes sur les plages, alors qu’elles sont aussi belles et diverses que des fleurs ! » Les rouges, oranges, violets, lui inspirent une palette de couleurs incroyablement nuancée.

C’est dans son atelier que se termine l’histoire de la création de L’Alguier de Gwen : portée par ses souvenirs depuis l’enfance des interminables étés à la mer en Bretagne, inspirée par son voyage à La Rochelle et à l’île de Ré, ainsi que par le vieil alguier de Concarneau, elle retrace les formes de plus de quarante espèces, sublime les couleurs que la nature leur a donné. Grâce à une technique aquarelle, ses algues translucides prennent vie : elles semblent flotter dans l’eau, dans leur milieu naturel.

En choisissant de présenter une vingtaine de ses dessins à travers le module « cabinet de curiosité » (à plaques coulissantes) du NANOmusée, elle souhaite que le public ait la liberté de composer ses propres bouquets, imaginer ses propres fonds marins, selon l’imaginaire et les envies de chacun. Chaque superposition de couleurs est unique.

Le module « microscope » complète l’ensemble en rappelant le travail des chercheurs comme Ingrid au sein du laboratoire de La Rochelle Université. La plaque représente une interprétation d’une cellule végétale, également dessinée par Gwen. Enfin, l’expérience qu’elle crée pour son module du NANOmusée comprend un enregistrement de sa propre voix : elle lit pour nous un texte imaginé et composé d'après les légendes à la fois scientifiques, poétiques et littéraires de son alguier des frères Crouan. « Conserver, enseigner et chercher sont les valeurs d’un alguier, j’y voyais sa poésie, il était déjà à la frontière de l’art. Et poésie encore à la lecture des légendes associées à la « florule », la tonalité des noms scientifiques, la localisation et le vocabulaire du rivage ».

Voyage polymorphe où l’on explore la couleur, où l’on remonte le temps vers la Bretagne du XIXème siècle, où l’on accompagne les chercheurs du laboratoire LIENSs de La Rochelle Université, L’Alguier nous propose une expérience riche et interactive, qui continue de vivre et d’évoluer selon le visiteur.

## Étude des molécules présentes dans les algues du littoral et de l'algoculture charentaise
 
À La Rochelle Université, une équipe du laboratoire Littoral Environnement et Sociétés (LIENSs) étudie comment valoriser les différentes molécules produites par les algues. Si ces végétaux marins peuvent être directement consommés et apporter d’excellents éléments nutritifs, il est également possible d’en extraire des composés qui trouvent des applications dans différents domaines, notamment celui de la santé. 

### Valoriser la biodiversité marine par les biotechnologies
Une partie des travaux de recherche menés au laboratoire LIENSs se fait dans le domaine des biotechnologies bleues qui vise à explorer la diversité chimique des organismes marins en vue de concevoir de nouveaux produits pour des secteurs variés (agronomie, nutrition, pharmacie, cosmétique, agroalimentaire).

Ce domaine des biotechnologies est très en prise avec le monde socio-économique. En effet, la plupart des recherches du laboratoire se font en partenariat avec des entreprises des secteurs concernés qui lui fournissent les ressources biologiques et bénéficient en retour de l’expertise des scientifiques pour leurs besoins d’innovations.

Dans ce domaine, les grandes algues apparaissent comme une ressource biologique particulièrement prometteuse.  En effet, ces végétaux marins sont largement distribués dans les zones froides, tempérées et tropicales. Ils jouent un rôle crucial dans le maintien des écosystèmes marins et génèrent de nombreux composés qui ont pour fonction par exemple de les protéger des effets de la lumière. 

### Les algues, une ressource pour la santé
Les algues présentent de multiples avantages pour la santé humaine. Consommées directement, comme des légumes, elles ont une grande valeur nutritionnelle : elles sont pauvres en lipides, riches en protéines, minéraux, vitamines, antioxydants et acides aminés essentiels. Des études ont montré qu’un régime riche en algues réduisait l’incidence de l’obésité, du cancer et des maladies cardiaques et cérébrovasculaires.

Les différents composés produits par les algues peuvent également faire l’objet d’innovations qui trouvent des applications bien spécifiques. Par exemple, les végétaux marins produisent des sucres comme l’agar, les carraghénanes et les alginates qui sont utilisés depuis des siècles par l’homme dans l’alimentaire pour leurs propriétés émulsifiantes et gelifiantes. 

En terme de valorisation, l’équipe de chercheurs et chercheuses du LIENSs travaille, entre autres, sur des applications thérapeutiques. En effet, les algues produisent un groupe de molécules appelé « polysaccharide », que l’on qualifie généralement de « sucre marin », et qui pourrait être utilisé dans le traitement des cancers du sein, particulièrement contre ses formes les plus aggressives. 

Les algues apparaissent donc comme une ressource biologique naturelle intéressante pour les industries qui les cultivent comme pour celles qui les transforment en produits de haute valeur. 
 
### L’algoculture sur l’île de Ré
Afin de créer et pérenniser une filière de valorisation des algues, il est important d’assurer en amont leur production et de sécuriser leur approvisionnement. Qu’elle soit destinée à la consommation directe d’algues ou à leur valorisation en laboratoire, l’algoculture est une activité qui progresse en France. En Charente-Maritime, l’équipe du laboratoire LIENSs travaille en étroite collaboration avec l’entreprise Algorythme, qui pêche et développe la culture des algues à Ars-en-Ré. 

Algorythme, habilitée et certifiée « impact neutre » par la direction départementale des territoires et de la mer pour le prélèvement, la récolte et l’exploitation des algues en Charente-Maritime utilise des méthodes respectueuses de l’environnement. Pour la récolte directe sur l’estran, les algoculteurs coupent les algues aux deux tiers de la plante, cette méthode stimule la production de spores et donc la reproduction des algues dans leur milieu naturel. Ainsi, par cette pratique, la partie du littoral exploitée a vu sa biodiversité se développer au fil des ans. 

A côté d’une pêche traditionnelle sur l’estran, Algoryhtme réhabilite d’anciens marais ostréicoles afin d’y développer une culture d’algues à partir de l’ensemencement de cordages. Cette méthode permettra ainsi d’avoir une production mieux contrôlée en quantité et en qualité. 

</br>



![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>


<a id="beton"></a> ![Header Beton](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/header_2_betonAcharne.png)

par Pauline Rolland, Yuri Zupancic, Emilio Bastidas-Arteaga et Rachid Cherif.

## Châpo
Béton acharné est un diptyque composé d’une sculpture et d’une œuvre vidéo, inspiré de l’utilisation du béton armé pour la construction de nos infrastructures et par le phénomène de corrosion. L’œuvre interpelle sur le rapport de force entre les constructions de l’homme et la nature. 

> D’APRÈS : la rencontre entre le couple d'artistes Pauline Rolland & Yuri Zupancic et les enseignants-chercheurs Emilio Bastidas-Arteaga et Rachid Cherif au laboratoire LaSIE (La Rochelle Université, CNRS)

> SUJET DE RECHERCHE : étude sur le phénomène de la corrosion qui dégrade les infrastructures en béton armé en zone littorale.

> LABORATOIRE D'ACCUEIL : Laboratoire des Sciences de l’Ingénieur pour l’Environnement (LaSIE) - UMR 7356 (La Rochelle Université, CNRS)

> Remerciements : Emilio Bastidas-Arteaga (La Rochelle Université), Rachid Cherif (La Rochelle Université), Wendy Laperrière (La Rochelle Université).

</br>

## Étude sur le phénomène de la corrosion qui dégrade les infrastructures en béton armé en zones littorales.

Le béton est un des matériaux les plus utilisés pour la construction. Cependant, sur les littoraux, les infrastructures en béton armé doivent faire face à un environnement qui leur est hostile. Au Laboratoire des Sciences de l’Ingénieur pour l’Environnement (LaSIE), l’équipe de Transferts, Dégradation et Valorisation des Matériaux (TDVM), étudie la durabilité du béton armé confronté au phénomène de la corrosion des armatures. 

### La corrosion des armatures
Les infrastructures en béton disposent d’une armature, généralement faite en acier. Cette technique permet de solidifier les constructions et d’améliorer leur résistance à la traction. Cependant, l’armature peut devenir la source de la détérioration du béton armé par la corrosion.

Le béton est un matériau poreux. Il est donc perméable aux différents agents agressifs de l’environnement. C’est notamment le cas des ions chlorures, présents dans le sel marin. Ces derniers pénètrent le béton et atteignent l’armature. À partir d’une concentration dite critique, le phénomène de la corrosion se déclenche et produit de la rouille à l’interface de l’armature et du béton. L’accumulation de rouille pousse le béton qui l’entoure jusqu’à le fissurer et l’éclater. Ces fissures accélèrent la pénétration des ions chlorures et entrainent une fragilisation croissante des structures. 

### Durabilité des matériaux de construction
Au LaSIE, l’équipe TDVM mènent des travaux de recherche sur l’exposition des matériaux de construction aux espèces agressives en zone littorale. Les moyens expérimentaux dont l’équipe dispose permettent de simuler les conditions que rencontrent les infrastructures en béton armé sur les bords de mer. 
Les travaux réalisés permettent de :

* Étudier la résistance des matériaux de construction exposés aux agents agressifs des zones littorales (chlorures, humidité). ;

* Suivre l’initiation de la corrosion des armatures et prédire leur durabilité. ;

* Tester de nouveaux matériaux/éco-matériaux qui pourraient entrer dans les normes de construction, tels que des bétons à base de granulats recyclés.

‍
### Prédire les mécanismes de dégradation
En parallèle et en complément des études expérimentales, l’équipe TDVM du LaSIE développe des modèles numériques permettant de simuler les mécanismes de dégradation. Ces modèles sont très utiles pour déterminer les solutions les plus durables et rentables pour préserver les infrastructures, améliorer leur conception, maîtriser les frais de maintenance et même formuler des mesures d’adaptation pour le changement climatique. 

En plus des facteurs propres aux constructions comme le type de béton utilisé ou l’âge de la structure, ces modèles permettent de prendre en compte les conditions climatiques et environnementales telles que l’humidité ou les températures qui exercent une grande influence sur la pénétration des ions chlorure et la corrosion. Ceci permet d’affiner les prédictions de durée de vie pour un site donné. 

Ces modèles permettent également d’estimer les effets du changement climatique qui sont très contrastés selon le site d’étude et le scénario climatique considéré. En effet, l’évolution du climat pourrait dans certains cas ralentir ou accélérer les dégradations. Ainsi, c’est une multitude de variables, propres à chaque environnement et à chaque construction, que les sociétés doivent prendre en compte pour choisir la meilleure stratégie d’adaptation face à la détérioration des infrastructures en béton armé. 


![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>


<a id="albatros"></a> ![Header Albatros](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/header_3_albatros.png)

par Mouawad&Laurier et Julien Collet.

## Châpo
Albatros, entre AIR et EAU, immense et majestueux oiseau, sentinelle des hautes mers, capable de parcourir des milliers de kilomètres sans mettre pied à terre, a inspiré aux artistes cette représentation de papier, à taille réelle. Une invitation à mieux connaître et protéger cette espèce extraordinaire. 


> D’APRÈS : la rencontre entre les artistes Mouawad&Laurier et le chercheur Julien Collet, porteur d’une Chaire de Professeur Junior au CEBC (La Rochelle Université, CNRS).

> SUJET DE RECHERCHE : suivi des populations des grands albatros dans les Terres Australes et Antarctiques Françaises.

> LABORATOIRE D'ACCUEIL : Centre d'Etudes Biologiques de Chizé (CEBC) - UMR 7372 (La Rochelle Université, CNRS).

> Remerciements : Charly Bost (CNRS), Julien Collet (La Rochelle Université), Cécile Ribout (CNRS).

</br>

## Suivi des populations des grands albatros dans les Terres Australes et Antarctiques Françaises
[Au Centre d’Etudes Biologiques de Chizé (CEBC)](https://www.cebc.cnrs.fr/), une [équipe de recherche s’intéresse aux prédateurs marins](https://www.cebc.cnrs.fr/predateurs-marins/). Étant situées dans la partie supérieure des réseaux alimentaires, ces espèces sont sensibles aux dynamiques des espèces situées dans les niveaux inférieurs. L’étude des prédateurs permet donc de connaitre l’état des écosystèmes auxquels ils appartiennent et de rendre compte de leurs évolutions, aujourd’hui souvent liées aux activités humaines. Parmi les espèces étudiées, une est emblématique des recherches qui se font au CEBC depuis des décennies : le grand albatros de l’archipel de Crozet _(Diomedea exulans)_. 

### Les études sur les albatros des Terres australes : un patrimoine scientifique
La première campagne sur les grands albatros de l’archipel de Crozet date de 1959. Les 200 albatros bagués lors de ces premiers travaux ont par la suite servi de base pour la mise en place d’un programme de suivi annuel des populations, dès 1965. Le CNRS et le Centre d’études biologiques de Chizé récupèrent la charge de ces recherches dans les années 1980, sous la houlette du l’Institut Polaire Français qui s’occupe de la mise en œuvre technique des missions dans les régions polaires.

Les scientifiques du CEBC peuvent donc s’appuyer sur des données de long terme issues d’un programme de suivi démographique annuel de presque 60 ans. Les équipes ont une très bonne connaissance des colonies de l’archipel de Crozet, à l’image de celle de l’île de la Possession, sur laquelle plus de 15 000 albatros ont été bagués et où tous les individus sont aujourd’hui identifiés. Au cours des décennies, les études sur le grand albatros ont également été pionnières dans [l’utilisation de nouvelles technologies de suivi des animaux](https://vimeo.com/260906546). À titre d’exemple, à la fin des années 1990, le grand albatros est la première espèce sur laquelle un traçage GPS a été testé.

Les équipes du CEBC poursuivent aujourd’hui ces travaux en envoyant des chercheur·euse·s pour des missions de 12 à 14 mois dans les Terres Australes Françaises afin d’assurer la continuité du suivi démographique et de mener diverses d’études sur le comportement des grands albatros.

### Cycle de vie et comportements alimentaires
Le programme de suivi démographique à long terme a permis aux scientifiques d’avoir une bonne connaissance des différentes phases de la vie des grands albatros. En effet, durant sa longue existence, 60 ans en moyenne, ces oiseaux marins passent par différentes étapes :
1. Juvénile : le jeune albatros est nourri les 8 à 9 premiers mois de sa vie par ses parents
2. Immature : il quitte le nid et passe entre 3 à 7 ans en mer sans revenir sur son île natale.
3. Adulte : l’albatros se reproduit pour la première fois vers l’âge de 10 ans.
4. Les périodes de reproduction induisent des comportements spécifiques.
5. À la fin de leur vie, les capacités des grands albatros sont diminuées.

Chaque période de la vie est caractérisée par des comportements alimentaires distincts : des oiseaux d’âges différents ne se nourrissent pas dans les mêmes zones. Connaitre et étudier l’évolution de ces comportements permet de comprendre des évolutions plus générales des écosystèmes. Par exemple, les grands albatros sont très dépendants des vents pour se déplacer, et donc pour trouver leur nourriture. Les chercheurs ont observé que les oiseaux reproducteurs font des voyages alimentaires de plus en plus courts pour nourrir les poussins. Cette évolution est à mettre en lien avec le changement climatique, qui entraine des vents plus forts autour de Crozet, facilitant le déplacement des albatros. Ainsi, en liant les données démographiques et les comportements alimentaires sur un long terme, les albatros sont bien une sentinelle qui permet de rendre compte de changements plus généraux des écosystèmes.

### Une sentinelle des activités de pêche
Depuis quelques années, les grands albatros font l’objet de nouvelles études. L’une des principales menaces qui pèse sur cette espèce est la pêche qui se pratique dans l’océan Austral. En effet, les bateaux dans cette région du monde utilisent des palangres : de longues lignes sur lesquelles sont attachés des hameçons qui portent des appâts. Les albatros sont attirés et plongent sur ces appâts et se prennent dans les hameçons par la même occasion.

Grâce à une [technologie de balises capables de repérer les radars des bateaux](https://www.cnrs.fr/fr/cnrsinfo/lenvolee-docean-sentinel-recompensee-par-leurope), tout en fournissant une localisation GPS, les scientifiques de Chizé ont étudié la distribution et la fréquence des rencontres entre les navires de pêche et les albatros. Par ailleurs, ces travaux ont également permis de détecter les bateaux qui pêchent illégalement, en comparant les zones de détection des radars aux positions des bateaux déclarés officiellement à un système de régulation international (système « AIS »). Ainsi, les études sur le grand albatros revêtent maintenant un intérêt pour les pays qui cherchent à avoir une meilleure connaissance et un meilleur contrôle des pêches dans leurs eaux.  

</br>


![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>


<a id="archeologie"></a> ![Header Archeologie](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/header_4_archeologieDuSilence.png)

par Emmanuel Faivre et Eve Lamendour.

## Châpo
L’artiste crée pour nous un paysage sonore en deux volets qui immerge les visiteurs dans un monde entre présent et futur : embarquez pour un voyage vers la Communauté d’Agglomération de La Rochelle en 2040.


> D’APRÈS : la rencontre entre l’artiste Emmanuel Faivre et les enseignantes-chercheuses Eve Lamendour et Louise Bernard du laboratoire EOLE (La Rochelle Université).

> SUJET DE RECHERCHE : La mise en récit, un outil pour mobiliser les citoyen•ne•s autour du projet La Rochelle Territoire Zéro Carbone. 

> LABORATOIRE D'ACCUEIL : EOLE (Environnement Organisation LEgislation) - (La Rochelle Université)

> Remerciements : Eve Lamendour (La Rochelle Université), Louise Bernard (La Rochelle Université).

</br>

## Emmanuel Faivre nous raconte sa démarche artistique:

À la lecture des premiers récits du laboratoire EOLE, témoignages fictionnels de l’année 2040, des paysages sonores se sont très vite imposés.
Avec la rencontre de Eve Lamandour et de Louise Bernard, j’ai exprimé mon envie de travailler à partir de paysages sonores que j’ai eu l’occasion d’enregistrer au début du premier confinement. J’ai réalisé ces enregistrements sur le territoire de l’agglomération de La Rochelle, un monde apaisé, où les sons des transports, des moteurs thermiques, de l’agitation de la foule n’existait plus… Ces paysages nous racontent un autre monde. Un monde de silence, un autre mode de vie : regarder et écouter le temps qui s’écoule.

Au début privé de liberté de circulation, j’écoutais et j’enregistrais ces SILENCES dans mon jardin, ces silences étranges… presque étrangers qui me faisaient voyager, entendre de lointains horizons, les découvrir, les retrouver. et puis j’ai commencé à enregistrer, à récolter ces sons préservés du territoire, que chacun a pu ressentir intimement, avec émotion, et en conserver le souvenir… loin du tumulte des choses.

C’est ce voyage que je vous propose aujourd’hui, dans lequel le sabot et le hennissement des chevaux, et les animaux ont pris leur place… Le Vivant.

Pour contraster et dialoguer avec ces paysages zéro carbone : une boîte de Pandore dans laquelle des casques nous donnent à écouter des paysages sonores qui n’existent plus dans un territoire zéro carbone, des sons d’un mode de vie révolu : les sons “carbonés", moteurs à explosion des bateaux, camions, voitures, motos, les axes de circulations et ses allers-retours incessants, les passages furtifs d’un avion, les caisses des super marché, aux bips tonitruants, des sonneries de smartphones et autres notifications, des sons qui en 2023 sont notre quotidien sonore.

La contrainte du NANOmusée : la scénographie implique différents modules et médiums. J’ai donc orienté mes choix pour que le travail sonore puisse cohabiter, coexister avec les autres.
J’ai alors réalisé deux créations sonores. L’une composée de paysages sonores “sobres et sensibles” diffusés de façon aérienne par des transducteurs intégrés aux mobiliers, qui ne se voient pas et s’intègrent avec les autres modules, l’autre composition sonore sous casque, invitant le spectateur-écouteur à une écoute attentive et individuelle, une écoute plus intime.

Cette expérience me donne l’envie de poursuivre ma pratique du sonore auprès de scientifiques et d’universitaires.

L’équipe qui travaille et nous accompagne sur ce projet NANOmusée est complice, et son accompagnement précieux et rassurant. Un Grand Merci à toutes et à tous, Salimata, Arthur, Cyril, Clément, Diego ainsi que Samuel et Gael, et toutes celles et ceux que j’oublie….

Un remerciement tout particulier à Eve Lamendour (La Rochelle Université), et Louise Bernard (La Rochelle Université).

---

</br>

SoundCloud des Paysages Sonores: [Archéologie du Silence, Paysages Ensevelis](https://soundcloud.com/manu-faivre/sets/nano-musee-2023-archeologie-du-silence-paysages-ensevelis)

---

</br>

## La mise en récit comme outil pour mobiliser les citoyens autour du projet La Rochelle Territoire Zéro Carbone

[La Rochelle Territoire Zéro Carbone (LRTZC)](https://www.larochelle-zerocarbone.fr/) est un projet porté par plusieurs acteurs de l’agglomération de La Rochelle qui vise la neutralité carbone sur ce territoire en 2040. Une équipe de recherche en sciences de la gestion du laboratoire EOLE a été sollicitée afin de réfléchir au moyen de mobiliser les citoyen•ne•s autour de cet objectif à travers la mise en récit.

### Le projet LRTZC
La Rochelle Territoire Zéro Carbone est un projet porté par cinq institutions fondatrices que sont la Ville de La Rochelle, la Communauté d’Agglomération de La Rochelle, La Rochelle Université, le Port Atlantique La Rochelle et l’Association Atlantech. L’objectif de ce consortium, lauréat en 2019 de l’appel à projet de l’Etat français baptisé « Territoires d’Innovation », est de faire de l’agglomération de La Rochelle le premier territoire français neutre en carbone en 2040.

La neutralité carbone est définie comme un équilibre entre les émissions et la capacité d’absorption des gaz à effet de serre par les puits de carbone sur un territoire. Cela signifie d’une part que les différents acteurs diminuent leurs émissions de dioxyde de carbone, et de l’autre que les environnements qui permettent de capter et de séquestrer les émissions soient préservés, voire développés.

Les actions se déploient sur plusieurs fronts : préservation du littoral et des marais pour accroitre leur capacité à séquestrer le carbone ; projets d’envergure portés par les institutions fondatrices, à l’image du quartier bas carbone envisagé par l’association Atlantech ; développement des énergies renouvelables et des mobilités douces pour réduire les émissions ; etc. Le projet LRTZC se distingue également par sa volonté de mobiliser les citoyens de l’agglomération autour de ces objectifs. Dans cette perspective, l’Université joue un rôle important avec notamment la [Chaire Participations Médiation Transition citoyenne](https://chaire-participations.recherche.univ-lr.fr/) ou encore des membres de l’équipe du laboratoire EOLE qui travaille sur la mise en récit.
 
### Les sciences de la gestion et la mise en récit
Les sciences de la gestion s’intéressent à la conduite et à l’organisation des collectifs humains tels que les entreprises, les associations ou encore les administrations en vue d’atteindre leurs objectifs. Dans cette perspective, l’art, et plus particulièrement la mise en récit, est apparue comme un outil pour atteindre certains buts. 

Un exemple emblématique est celui d’EDF. À la fin des années 1970, l’arrivée d’une nouvelle technologie, la télématique, pose de nombreuses questions sur l’organisation de certains services. Contraintes par des enjeux politiques, communicationnels et des divisions internes, l’entreprise ne parvient pas à construire une stratégie sur ce sujet. Un groupe de chercheurs propose alors de contourner le blocage par la mise en récit d’une entreprise fictive, dans un pays fictif, qui intègre la télématique dans sa stratégie. Ce travail a donné lieu à la publication d’un ouvrage, [Les chroniques muxiennes](https://gallica.bnf.fr/ark:/12148/bpt6k33704545), et a permis à l’entreprise de se saisir de la question de la télématique et de l’intégrer dans sa stratégie d’entreprise.

### LRTZC et la mise en récit
L’équipe de recherche du laboratoire EOLE part d’un constat : la diffusion d’informations scientifiques, en l’occurrence sur l’urgence climatique, ne provoque que peu d’effets sur le comportement des citoyen•ne•s. Ainsi, l’objectif de rendre l’agglomération de La Rochelle neutre en carbone pour 2040 doit s’accompagner d’autre chose pour rendre ce futur désirable et partagé par la population.

[La proposition des scientifiques est de créer des récits](https://videos.univ-lr.fr/video/2345-recit-et-storytelling-dans-la-conduite-du-changement-a-propos-du-projet-la-rochelle-territoire-zero-carbone/) mettant en scène des habitants de l’agglomération en 2040 sur ce territoire neutre en carbone. Les premiers récits, écrits par les chercheur•euse•s, racontent par exemple la vie d’un agriculteur, d’une conchylicultrice ou encore d’une jeune écolière. Les textes racontent leur quotidien et leurs activités : la manière d’élever des bovins et de produire de l’énergie pour l’agriculteur, la coopération pour assurer la qualité des eaux pour la conchylicultrice et une journée de cours au marais de Tasdon à observer la nature pour la jeune écolière.

La démarche étant participative, les habitants de l’agglomération sont également invités à prendre part à la mise en récit. C’est notamment le cas lors d’ateliers avec des lycéen•ne•s. Certaines informations leur sont communiquées et iels doivent imaginer des histoires qui se passent en 2040.

![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>


<a id="observatoire"></a> ![Header Observatoire](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/header_5_observatoire.png)

par Junie Briffaz et Sophie Laran.

## Châpo
L’Observatoire, inspiré de l’expérience immersive de l’artiste au sein du laboratoire PELAGIS, nous emmène à la rencontre de l’extraordinaire diversité des oiseaux, des mammifères marins et des autres mégafaunes évoluant au large de la côte atlantique.


> D’APRÈS : la rencontre entre l’artiste Junie Briffaz et l’ingénieure de recherche Sophie Laran ainsi que l’équipe de l’unité d’appui à la recherche PELAGIS (La Rochelle Université, CNRS).

> SUJET DE RECHERCHE : observation de la mégafaune marine et suivi des échouages.

> LABORATOIRE D'ACCUEIL : PELAGIS - UAR 3462 (La Rochelle Université, CNRS).

> Remerciements : Sophie Laran (La Rochelle Université), Olivier Van Canneyt (La Rochelle Université), Ghislain Doremus (La Rochelle Université), Paula Méndez Fernandez (La Rochelle Université) et l’ensemble de l’équipe de PELAGIS.

</br>

## Observation de la mégafaune marine et suivi des échouages
L’observatoire [PELAGIS](https://www.observatoire-pelagis.cnrs.fr/) a pour mission le suivi des populations de mammifères marins, d’oiseaux de mer et autres espèces marines de grande taille (la mégafaune marine). Les connaissances développées par PELAGIS sur ces différentes espèces place l’observatoire au cœur des enjeux de conservation et de protection de la biodiversité marine. De plus, l’observatoire s’occupe d’étudier la mortalité des mammifères marins à travers la coordination du Réseau National Échouages (RNE).

### Les campagnes d’observation
Les campagnes de suivi de la mégafaune marine menées par PELAGIS se font de deux manières. La première méthode consiste à mener des [observations depuis les airs à l’aide de petits avions](https://www.observatoire-pelagis.cnrs.fr/suivis-en-mer/suivis-aerien/). Ces derniers disposent de hublots qui permettent aux membres de l’équipe d’observer la surface de l’eau. Dès que les scientifiques repèrent un animal, un bateau ou un déchet, il est signalé aux autres membres qui disposent d’un ordinateur et qui enregistrent chaque observation afin de pouvoir les rentrer dans une base de données une fois de retour dans les bureaux.


PELAGIS mène aussi des campagnes d’[observation en mer](https://www.observatoire-pelagis.cnrs.fr/suivis-en-mer/suivis-par-bateau/) grâce à une collaboration avec l’Institut Français de Recherche pour l’Exploitation de la Mer (IFREMER). Cela permet d’embarquer des observateurs sur les bateaux de la Flotte océanique française à l’occasion de différentes campagnes dans le golfe de Gascogne, dans la Manche ou encore en Méditerranée.

Il faut noter que l’avion dispose de certains avantages sur les campagnes en bateau. Les missions de survol permettent de couvrir une plus large zone en moins de temps. De plus, l’avion peut se déplacer facilement vers des zones où la météo est plus convenable pour mener les observations.

### Les enjeux de protection et de conservation
Les activités de l’observatoire PELAGIS rencontrent à différents niveaux les enjeux de conservation et de protection des oiseaux et mammifères marins. D’abord, les observations permettent de connaitre l’état des populations pour lesquelles les États se sont engagés dans des programmes de protection et de conservation. Deuxièmement, certaines campagnes PELAGIS sont réalisées dans des zones où les activités humaines sont multiples et exercent des pressions sur la mégafaune marine. C’est notamment le cas du Parc naturel marin Estuaire de la Gironde et de la mer des Pertuis pour lequel l’observatoire a mené des missions de survol. Cette zone a en plus le statut d’aire marine protégée et pourrait faire l’objet de réglementations en matière de protection de la biodiversité.  La connaissance du milieu marin et de l’impact des activités humaines revêt donc un intérêt particulier.

Finalement, la connaissance des populations d’animaux marins permet de comprendre l’ampleur des crises lorsqu’elles surviennent. C’est par exemple actuellement le cas avec les échouages massifs de dauphins communs sur les côtes atlantiques. Connaitre le nombre d’individus vivants permet de se rendre compte de l’impact des mortalités hivernales qui sont enregistrées sur les côtes.

### Le Réseau National Échouages
L’observatoire PELAGIS est également le coordinateur du [Réseau National Échouages](https://www.observatoire-pelagis.cnrs.fr/echouages/suivis-des-echouages/). À ce titre les équipes du laboratoire reçoivent les signalements de leurs correspondants dans les régions littorales ou des promeneurs qui trouvent un cadavre. Cela permet d’une part de recenser les échouages en fonction des zones et des espèces. Puis, les équipes procèdent à un examen des animaux afin de récolter des informations sur leur état de santé et de leur environnement. Ce réseau, vieux de plus de 50 ans et basé en partie sur le concept de science participative, constitue la principale source des connaissances sur les mammifères marins en France.

De plus, les autopsies permettent de déterminer les causes de décès des mammifères. Dans le cas des échouages de dauphins qui ont eu lieu dernièrement, il s’avère que la majorité des individus sont morts suite à des blessures causées par des engins de pêche.

![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

<br>

<a id="fenetres"></a>![Header fenetres](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/header_6_fenetresSurMer.png)

# Fenêtres sur Mer

par Valérie Ballu, Alain Gaugue, Thibault Coulombier et Anouck Boisrobert

## Chapô

Par le déploiement de quatre paysages de papier alliant superpositions et jeux d’ombres, Fenêtres sur mer nous invite à découvrir la diversité des études permises par l’utilisation du drone marin PAMELi. Les scènes découpées vivent avec le regard de l’observateur, et l’embarquent sur le robot flottant pour explorer le littoral. 

* Technique : pop-up, diorama, jeux d’ombres, superposition.

* Matériau : papier calque, papier de couleur, carton plume.

> D’APRÈS : la rencontre entre l’artiste Anouck Boisrobert, la chercheuse Valérie Ballu (LIENSs), l’enseignant-chercheur Alain Gaugue (L3i) et l’ingénieur d’étude Thibault Coulombier (LIENSs et L3i).

> SUJET DE RECHERCHE : Le drone marin PAMELi, outil innovant pour une meilleure compréhension des écosystèmes littoraux.

> LABORATOIRE D'ACCUEIL : LIENSs (Littoral Environnement et Sociétés) – UMR 7266 
L3i : Laboratoire Informatique, Image et Interaction

<br>

## Faire de la science autrement

Le laboratoire Littoral Environnement et Sociétés (LIENSs) et le laboratoire Informatique, Image et Interaction (L3i), ont uni leurs compétences pour mettre au point un nouvel outil permettant de mieux saisir les dynamiques des littoraux et des océans : le drone marin PAMELi.

L’outil conçu par les laboratoires de La Rochelle Université et du CNRS s’inscrit dans une volonté de « faire de la science autrement ». En effet, les drones marins présentent de nombreux avantages économiques, écologiques et permettent une collaboration croissante entre les disciplines scientifiques. C’est le cas du drone marin PAMELi.

Equipé d’une diversité de capteurs, ce drone peut réaliser différentes mesures de manière simultanée (température, profondeur, salinité, etc). Chaque sortie en mer permet ainsi de mutualiser les efforts et de fournir des données pour des projets de recherche variés. Ce drone propulsé par électricité a une empreinte carbone réduite et peu d’impact sonore sur l’environnement marin par rapport aux bateaux habituellement utilisés. 

D’un point de vue scientifique, PAMELi est un outil innovant pour le développement de recherches interdisciplinaires grâce à la coopération qu’il génère dans la collecte de données. Ces actions mutualisées sont aujourd’hui reconnues afin d’avoir une compréhension globale de l’évolution des littoraux, dans le contexte de changement climatique. Les informations enregistrées par PAMELi sont automatiquement stockées dans une base de données communes. Ce fonctionnement facilite ainsi la visualisation, l’analyse, le partage et le réemploi des informations collectées par ce drone. 

## Un exemple des mesures permises par ce drone : le niveau marin

Le niveau de la mer résulte de plusieurs phénomènes : la marée, le vent, les vagues, les fonds marins, la pression atmosphérique jouent sur la hauteur de nos océans. Comprendre le niveau marin nécessite donc de nombreuses mesures. Grâce aux différents instruments embarqués sur PAMELi (station météo, sondeur bathymétrique, mini-cyclopée), il est possible de mesurer de manière précise et localisée le niveau de la mer. 

L’un des principaux intérêts techniques du drone est de compléter et corriger les mesures faites par les satellites, ces derniers permettant de mesurer le niveau des océans sur l’ensemble du globe. Grâce à la capacité du drone à naviguer très près des côtes, PAMELi apporte des données complémentaires sur le niveau des mers en zones littorales. L’outil mini-cyclopée dont il est équipé permet également de corriger les mesures effectuées par satellites. Ce système combine un instrument mesurant l’altitude du drone par rapport à la surface de l’eau (altimètre) et un système GPS qui enregistre l’information sur sa position aux satellites. En croisant ces deux données, il offre des informations précises sur le niveau de la mer localement et nous renseigne sur le phénomène de montée des eaux. 

## Des drones intelligents

Les équipes scientifiques ont développé des drones marins innovants qui se pilotent avec la plus grande autonomie possible. Ils peuvent se commander jusqu’à plusieurs dizaines de kilomètres depuis la côte ou d’un bateau et peuvent suivre un plan de navigation précis afin de cartographier une zone définie.

A l’image des voitures autonomes, les drones enregistrent différentes données de leur environnement pour corriger leur trajectoire. En mer, ils doivent progresser dans un milieu en constante évolution et prendre en compte des obstacles (objet à la surface, navire pénétrant la zone). Les différents capteurs (radar, lidar, caméra, sonar) dont ils sont équipés leurs permettent de s’adapter instantanément. L’objectif à terme est de développer une « meute » de drones : plusieurs engins qui fonctionnent de manière synchronisée et s’échangent des informations en temps réel. 

Ces travaux de recherche se poursuivent et permettent de renforcer les performances des drones, favorisant une meilleure compréhension des écosystèmes littoraux et marins.   


</br>



![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>
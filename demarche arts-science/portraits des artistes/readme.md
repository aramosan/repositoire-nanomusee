
<details open>
<summary>Artistes</summary>

   * [Gwen Le Gac](#gwen)
   * [Yuri Zupancic & Pauline Rolland](#yuriPauline)
   * [Mouawad & Laurier (HandCoded)](#mouawadLaurier)
   * [Emmanuel Faivre](#emmanuel)
   * [Junie Briffaz](#junie)
   * [Anouck Boisrobert](#anouck)

</details>
</br>

---

<a id="gwen"></a>

# Gwen Le Gac


![Portrait de Gwen](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/demarche%20arts-science/portraits%20des%20artistes/GwenLeGac_image.png?ref_type=heads)

Gwen Le Gac est née en Bretagne. Vit en Seine-Saint-Denis.
Après des études en Arts Plastiques, Histoire de l’Art et Arts Décoratifs, elle navigue aujourd’hui dans l’univers de la création.
Auteure, illustratrice, plasticienne, elle n’a de cesse d’expérimenter tout médium et imagine, pour chaque projet, de nouvelles formes narratives et graphiques, elle explore l’imaginaire et interroge le rapport au réel.


[http://www.gwenlegac.fr/](http://www.gwenlegac.fr/)

Instagram: [@ateliergwenlegac](https://www.instagram.com/ateliergwenlegac)



---

### Module: [L'Alguier](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/demarche%20arts-science/demarche%20des%20chercheurs-artistes?ref_type=heads)

### Parole d'artiste:
« Le projet du NANOmusée m’a immédiatement touché, je suis née dans un tout petit village en Bretagne, éloigné de l’Art et de la culture en général.
De mon enfance contemplative, la nature symbolise mes premiers chocs artistiques.  
Si on ne peut aller vers l’art, que l’art vienne à nous, c’est tout ce que je souhaite pour la vie du NANOmusée, que l’art soit nomade. Et ouvert à tous, pour un art débarrassé de son étiquette élitiste. Que le NANOmusée soit une passerelle entre l’art et la vie.

Mon travail de création est une recherche constante de lien entre le fond et la forme, et chaque fois je fais table rase et cherche à trouver la forme qui épousera parfaitement le fond. Et vice-versa.
La rencontre entre l’Art et la Science a été très naturelle, je me sens moi-même chercheuse, je me suis largement inspirée du vocabulaire formel de la recherche, l’éclairage, lumière blanche, aseptisée.
L’idée de la vue de l’algue au microscope est née au laboratoire, pendant la résidence, en interaction avec un chercheur. »

![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>

# Yuri Zupancic & Pauline Rolland <a id="yuriPauline"></a>


![Portrait de Yuri Zupancic](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/demarche%20arts-science/portraits%20des%20artistes/Yuri%20-%20Pauline_image.png?ref_type=heads)


Pauline Rolland et Yuri Zupancic vivent et travaillent à Paris. Ils ont chacun une pratique artistique individuelle. Pauline, peint, dessine et plus récemment filme. Yuri crée principalement des peintures, notamment miniatures sur des micropuces et des touches d’ordinateur.

Ils collaborent ensemble depuis 2015 sur divers projets aux techniques variées. Ensemble, ils explorent la vidéo, l’installation, la photographie, la peinture et la sculpture. Ils s’interrogent sur la place de l’homme dans la société et sur la planète Terre à l’aune des changements qui bousculent notre monde.

En 2019, ils participent à deux résidences d’artistes dans le Colorado et créent une série d’installations autour de la personnification de Mother Nature. Le public est invité à prendre part à celle-ci de manière immersive. Les artistes cherchent par ce biais à positionner le spectateur en réel acteur : une manière pour eux d’interagir avec lui, mais également de provoquer une réflexion commune.

Pour la Nano-Musée de l’Université de la Rochelle, Pauline Rolland et Yuri Zupancic ont souhaité travailler sur la question de la corrosion du béton armé, car celle-ci entrait en résonance avec leurs autres sujets de recherche entre activités humaines, nature à préserver et future à construire.


[Site web Yuri Zupancic](https://yuri-z.com/)


---

### Module: [Béton Acharné](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/demarche%20arts-science/demarche%20des%20chercheurs-artistes?ref_type=heads)

### Parole d'artiste:
« La rencontre avec les chercheurs a été déterminante : les rencontrer, échanger et apprendre sur les divers aspects de leur travail. Nous avons abordé tous types de questions : technique, scientifique, chimique, politique, philosophique. 

À la fin de notre rencontre, nous avons notamment discuté de notre rapport à la nature et de comment celle-ci par le vent, les embruns… abîmait les structures. Comment le réchauffement climatique accélérait la corrosion ? Comment, en tant que chercheurs, leurs missions étaient de trouver des solutions pérennes pour construire durablement des infrastructures nécessaires à améliorer la qualité de vie de l’homme. 

Cette discussion a été notre point de départ, puis c’est un dialogue sans interruption qui s’est créé entre nous : sur notre rapport au temps, à la nature, à la manière dont nous, êtres humains devons protéger notre planète en ayant en tête que nous ne sommes que de passage sur terre. L’idée du pont, de la transmission et des silhouettes, s’est très vite imposée. Ces échanges nous ont nourri et nous ont donné envie de positionner directement notre travail comme un complément du leur ».

![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>
 
<a id="mouawadLaurier"></a>

# Mouawad + Laurier (HandCoded)



![Portrait de Mouawad&Laurier](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/demarche%20arts-science/portraits%20des%20artistes/Mouawad%20-%20Laurier_image.png?ref_type=heads)


Depuis 2002, Cyril et Maya s'entrelacent dans une symbiose entre l'art et la technologie, sculptant le son et peignant la lumière. Leur duo, parfois baptisé Mouawad + Laurier, parfois Hand-Coded. Hand-Coded incarne leur expertise d'ingénieurs-chercheurs en art et technologie, tandis que Mouawad + Laurier devient leur atelier d'expression artistique. Mais choisir entre ces deux identités serait réducteur. Elles fusionnent, se mêlent, et se démêlent au gré des récits à raconter et des expériences à vivre, se nourrissant des rencontres, des aspirations mutuelles, et des nécessités du moment.


C'est ainsi que nous scrutons la technologie avec passion et discernement, interrogeant notre rôle en tant qu'êtres humains. Quelle étrange priorité accordons-nous à la technologie, la considérant comme la solution à tous les maux que nous avons engendrés ? Est-elle la cause ou la conséquence de nos dilemmes contemporains ? Et pourtant, malgré nos questionnements, nous aimons la technologie, captivés par son potentiel qui nous fait nous sentir semblables à des divinités, marionnettistes de nos ordinateurs, insufflant vie à des créations autonomes qui se déploient à partir des données qui lui servent d’aliment.

Nous sommes artisans de machines vivantes, mais elles nous murmurent que notre attention devrait se porter sur la nature, sur le vivant qui détient les clés de notre salut. Nous ne protégerons pas le vivant en le submergeant de technologies. Comment alors émouvoir et guider un public fasciné par la technologie vers la contemplation de la nature ? Comment toucher l'inconscient collectif ?

Nos réflexions nous ramènent aux racines et aux cultures, car la technologie semble prétendre éclipser tout ce qui a précédé l'ère moderne. Est-ce que tout ce qui préexistait à elle est désormais primitif ? Pourtant, notre passé nous a façonnés, et la culture ainsi que la philosophie sont des ancres, des sources de réflexion et de connexion avec notre être, avec autrui, avec notre héritage. Finalement, qu'est-ce qui importe réellement ? Pour nous, l'essentiel réside dans la continuité d'un récit, plutôt que dans la rupture.


[Site Web de Mouawad&Laurier](https://mouawadlaurier.com)

Instagram: [@mouawad.laurier](https://www.instagram.com/mouawad.laurier)

[Site Web de HandCoded](https://www.hand-coded.net/)



---

### Module: [Albatros](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/demarche%20arts-science/demarche%20des%20chercheurs-artistes?ref_type=heads)

### Parole d'artiste:
« L’idée de l’œuvre Albatros est née progressivement, mais la conversation clé fut la rencontre avec les chercheurs, qui nous ont énuméré toutes les choses exceptionnelles sur cet oiseau : le fait qu’ils soient capables de voyager sur des milliers de kilomètres, pendant des années, sans toucher terre. Les parades nuptiales pour trouver leur partenaire - avec lequel ils restent toute la vie. Leur utilité en tant que sentinelle quand ils aident à détecter les bateaux de pêche illégaux grâce aux balises que les scientifiques placent sur eux pour les étudier, etc.

On souhaite une longue vie au NANOmusée. C’est un format très prometteur et un formidable partage de connaissances qui permet d’amener le Musée dans des lieux reculés, dans des écoles, dans la rue. C’est un chouette outil de démocratisation de l’Art, on adore et on est déjà engagé dans ce mouvement qui, nous pensons, va prendre de plus en plus d’ampleur dans l’avenir. La culture ne doit pas rester enfermée et le NANOmusée est une belle manière de la faire sortir ».

![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>

<a id="emmanuel"></a>

# Emmanuel Faivre
Preneur de Sons . Créateur sonore et visuel


![Portrait de Emmanuel Faivre](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/demarche%20arts-science/portraits%20des%20artistes/EmmanuelFaivre_image_2.jpeg?ref_type=heads)


### Mon Parcours

Mon approche du sonore est sensible, affective et chargée de souvenirs par la pratique d’enregistrements, de montages et de mixages sonores dès l’âge de 11 ans.

Preneur de sons, ingénieur du son, créateur visuel (mapping) et sonore, mon expérience se construit par de nombreuses créations avec des compagnies de spectacle vivant. Tout d’abord avec Jacques Bailliard, Théâtre de Saône et Loire, Jacques Fornier puis avec Catherine Dasté à Pernand Vergelesse, j’apprends le Théâtre de la décentralisation, le Théâtre de Tréteaux, avec du Théâtre dans les cuveries, les caves, les lavoirs... mais aussi dans les CDN, les Scènes Nationales... puis je rencontre Le Porte Plume, le Théâtre de l’Index, Théâtre en Seine, le Centre dramatique de La Courneuve, Cie 36 du mois, le Théâtre de La Chaloupe, Le Moulin Théâtre, La Martingale, Les Tréteaux de France, Les Musiques de La Boulangère, Cie El Ajouad... Complice également de la scénographe Véronique Bretin avec de nombreuses installations sonores pour la muséographie. Mon expérience se tourne aussi vers l’audiovisuel avec des montages son et des mixages pour des courts-métrages et des documentaires audiovisuels : La nature en héritage, Zoo in situ, Le Percent, La Colère dans le vent, Le secret, Angèle ça la casse, Le Bourdon, Vivement Lundi. Je développe en 1996 avec ACIRENE – Elie Tête, une méthodologie d’enregistrements de paysages sonores en quadriphonie, pour étudier et valoriser des acoustiques sensibles de territoire, pour conserver une trace de l’état acoustique ”initial” de ceux-ci. Par la suite, dès que possible, j'enregistre des P@ysages Sonores, des sons seuls, en résidence comme en voyage. Je constitue une sonothèque personnelle qui devient la source d’inspiration de mes créations sonores. Je dépose nombre de mes P@ysages Sonores sur une carte collaborative, radioaporee.org, qui recueille des paysages sonores du monde entier et les ore en écoute.

En parallèle de mon travail personnel, j’aime échanger et partager mon expérience. J’ai animé divers ateliers et suis intervenu à plusieurs reprises, en milieu scolaire et avec des adultes, Collège à Gasny en 2019 avec Les Tréteaux de France, Maison de quartier de Port Neuf avec “Jeux Sonores” en 2021 pour il convito, CLEA en Pays de Saint Omer en 2022 et CLEA en pays de Mormal en 2023 : partages de gestes artistiques avec des partenaires écoles primaires, collèges, lycées, EHPAD, Services Santé, SESSAD... mais aussi ateliers de médiations dans le cadre de Réseaux Connectés, développer l’écoute par l’enregistrement de paysages sonores de son quartier, de son territoire.

### Ma Démarche Artistique

_« C’est beau un jardin qui ne pense pas encore aux hommes »_ - Jean Anouilh

Le son est avant tout une sensation. Se mettre en posture d’écoute, c’est une forme d'attention à l’autre, aux autres, à ce qui nous entoure. Dans la vie de tous les jours comme en situation de prises de sons, être prêt à recevoir, sans attente. Prendre le temps.

Travailler les sons pour et par leur matière, leur corps, dans le temps et dans l’espace. J’ai une prédilection pour la multidiusion, diusion sur de multiples hauts parleurs.
J’aime à faire des séances d'écoute de P@ysages Sonores en quadriphonie, ces séances
développent fortement notre rapport et notre attention à l’écoute au quotidien.

[Site Web de Emmanuel Faivre](https://emmanuelfaivre.net/)

emmanuelfaivre.contact@gmail.com

+33 (0)6 72 76 86 8


---

### Module: [Archéologie du Silence](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/demarche%20arts-science/demarche%20des%20chercheurs-artistes?ref_type=heads)

### Parole d'artiste:
« À la lecture des premiers récits du laboratoire LITHORAL, témoignages fictionnels de l'année 2040, des paysages sonores se sont très vite imposés. Avec la rencontre des chercheuses Eve Lamendour et Louise Bernard, j’ai exprimé mon envie de travailler à partir de paysages sonores que j’ai eu l’occasion d’enregistrer au début du premier confinement. J’ai réalisé ces enregistrements sur le territoire de l’agglomération de La Rochelle. Un monde apaisé, où les sons des transports, des moteurs thermiques, de l’agitation de la foule n'existait plus... Ces paysages nous racontent un autre monde. Un monde de silence, un autre mode de vie : regarder et écouter le temps qui s’écoule.

La contrainte du NANOmusée : la scénographie implique la juxtaposition de différents modules et médiums. J’ai donc orienté mes choix pour que le travail sonore puisse cohabiter, coexister avec les autres. J’ai alors réalisé deux créations sonores. L’une composée de paysages sonores “sobres et sensibles” diffusés de façon aérienne par des transducteurs intégrés aux cubes, qui ne se voient pas et s'intègrent avec les autres modules ; l’autre composition sonore sous casque, invitant le spectateur-écouteur à une écoute attentive et individuelle, une écoute plus intime.

Cette expérience me donne l’envie de poursuivre ma pratique du sonore auprès de chercheurs et d’universitaires. L’équipe qui développe le NANOmusée est complète et complice, et son accompagnement précieux ».

![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>

<a id="junie"></a>

# Junie Briffaz

![Portrait de Junie Briffaz](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/demarche%20arts-science/portraits%20des%20artistes/JunieBriffaz_image.jpg?ref_type=heads)

Junie Briffaz, artiste et illustratrice. Née à Mantes la Jolie. Vit et travaille à Toulouse. Travaille l'image imprimée, le dessin, et la peinture acrylique. Son travail s'inspire des arts populaires, comme la bande dessinée franco-belge, les jeux vidéo ou encore l'art naïf. Elle dessine un monde vaste, magique, parfois dangereux, et raconte des histoires dans des saynètes aux personnages multiples et miniatures. Des scènes teintées d'ironie et parfois même de cruauté, qui peuvent parfois assombrir son monde pourtant très coloré. Suite à son DNSEP à l’isda Toulouse en 2013, le festival de bande dessinée de Colomiers lui decerne le prix jeune talents Midi-Pyrénées 2015. Elle participe à plusieurs expositions et notamment à La cuisine, centre d’art et de design, à Nègrepelisse (2013) à la Fisher Parrish Gallery, à New-York (2018), et à la MAGCP de Cajarc (2019).
(image de Junie Briffaz)



[Site Web de Junie Brifffaz](https://juniebi.tumblr.com/)

Instagram: [@junie_briffaz](https://www.instagram.com/junie_briffaz)

---

### Module: [L'Observatoire](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/demarche%20arts-science/demarche%20des%20chercheurs-artistes?ref_type=heads)

### Parole d'artiste:
« L'idée de l’œuvre L’Observatoire est née avant la rencontre avec les chercheurs. En me renseignant sur les activités du laboratoire, j'ai fait plusieurs propositions. Cependant, suite à ma résidence à au laboratoire PELAGIS, une de mes propositions a évolué. Cette résidence m'a permis de me rendre compte de la réalité des actions menées par le laboratoire. Des activités très diverses et qui demandent des compétences très différentes. J'ai eu envie de retranscrire ces différentes facettes puisque PELAGIS ne peut pas être réduit à une seule activité à mon sens. J'ai ressenti le besoin de raconter ce que j'avais vu et vécu.

J'ai adoré la rencontre avec les chercheurs, je n'ai pas souvent l'occasion de rencontrer des univers aussi différents du mien. Et je suis très reconnaissante à l'équipe du NANOmusée ainsi qu'à mon métier de me permettre de découvrir le monde de la recherche et celui de l'université. Je trouve ça toujours très intéressant d'élargir ses horizons et de se nourrir ailleurs.

J'ai fait un rapprochement entre le métier de chercheur et le mien, dans le sens d'un investissement total dans son travail, et celui d'essayer des choses qui n'aboutiront pas ou qui n'auront pas forcément de finalité concrète. Par contre c'est vrai que j'ai été surprise par la dimension humaine et vitale pour la mégafaune marine. Surtout pour la partie intervention et échouage ».

![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>

<a id="anouck"></a>

# Anouck Boisrobert


![Portrait de Anouck Boisrobert](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/demarche%20arts-science/portraits%20des%20artistes/AnouckBoisrobert_image.jpg?ref_type=heads)


Anouck a passé son enfance dans les Pyrénées et au bord de l’océan Atlantique. Depuis qu’elle sait tenir une paire de ciseaux elle bricole des univers en papier et invente des histoires qu’elle dessine, c’est donc naturellement qu’elle a fait des études d’illustration à Paris puis à Strasbourg. À l’occasion d’un Workshop sur la technique du pop-up, elle s’associe avec Louis Rigaud pour créer la maquette de leur premier livre Popville qui sortira un an plus tard aux éditions Hélium. Ils réalisent ensemble plusieurs livres en pop-up dont Dans la forêt du paresseux et Océano. Anouck travaille comme illustratrice sur différents projets et anime régulièrement des ateliers autour de ses livres.
L’esprit ludique et la manipulation se retrouve dans son travail personnel, où elle aime jouer avec les formes et les transparences de couleur. 




[Site Web de Anouck Boisrobert](https://anouckboisrobert.fr/)

Instagram: [@anouck_boi](https://www.instagram.com/anouck_boi/)

FaceBook: [anouck.et.louis](https://www.facebook.com/anouck.et.louis)

---

### Module: [Fenêtres Sur Mer](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/demarche%20arts-science/demarche%20des%20chercheurs-artistes?ref_type=heads)

### Parole d'artiste:
« J’ai l’habitude de travailler à partir du format d’un livre et ce qui m’a intéressé ici était justement d’en sortir pour mettre en scène mes images dans ces grands volumes cubiques. Je souhaitais garder l’effet de surprise que l’on a avec les pages d’un livre en pop-up que l’on déplie et l’idée d’un jeu d’ombre et de lumière s’est progressivement précisée.

J’ai réalisé beaucoup de tests et de maquettes avec différents papiers, comme des petits théâtres d’ombres à poser devant une lampe. J’ai essayé de rester simple à chaque visuel, de ne garder que l’essentiel. Souvent ce sont des mots ou des petits bouts de phrases extraites de mes prises de notes lors de la rencontre avec l’équipe de chercheurs qui m’ont inspirée.

J’ai voulu faire ressortir quatre thématiques de leur travail pour aborder la pluralité de leur recherche et je trouvais que l’idée d’images qui apparaissent et prennent leur sens grâce à une source lumineuse illustrait plutôt bien la mission d’une recherche scientifique. »



![footer section](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/raw/principale/repositoire%20-%20assets/mediaWiki/footer_sections.png)

---

</br>



# À propos de ce repositoire

## Sommaire
<details open>
<summary>Sommaire</summary>

* [Qu'est-ce qu'un repositoire ?](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#quest-ce-quun-repositoire)
* [Comment naviguer sur ce repositoire ?](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#comment-naviguer-sur-ce-repositoire)
    * [Concernant la version 1 et la version 2](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#concernant-les-versions-1-et-2-du-nanomus%C3%A9e)
* [Comment contribuer ?](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#comment-contribuer-%C3%A0-ce-repositoire)
    * [« J'ai une idée/un fichier, mais je ne suis pas doué.e en informatique »](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#-jai-une-id%C3%A9e-ou-jai-un-fichier-dune-nouvelle-pi%C3%A9ce-3d-par-exemple-%C3%A0-apporter-mais-je-ne-suis-pas-trop-dou%C3%A9-en-informatique-)
    * [« J'ai des connaissance en informatique et/ou j'ai envie de m'engager activement dans la contribution »](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#-jai-des-connaissances-techniques-en-informatique-etou-je-suis-pr%C3%AAt-%C3%A0-mengager-activement-dans-la-collaboration-)
    * [Fonctionnement technique du repositoire](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#fonctionnement-technique-du-repositoire)
        * [Origine et portée de GIT](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#origine-et-port%C3%A9e-de-git)
        * [GitLab](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#gitlab)
    * [Commencer à contribuer : « Cloner » le repositoire](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#commencer-%C3%A0-contribuer-cloner-le-repositoire)
        * Étapes
* [License](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee#license)

</details>

## Qu'est-ce qu'un repositoire?

Bienvenue sur la documentation en ligne du NANOmusée ! Ce dépôt open-source a pour vocation de mettre à disposition des institutions, structures, bricoleur.euse.s, et du grand public, toutes les ressources nécessaires pour, d'une part, comprendre le projet NANOmusée de La Rochelle Université dans son intégralité (gestion, conception artistique et scientifique, communication et valorisation) et, d'autre part, permettre la fabrication des modules de manière autonome et la conception d'outils de médiation.

Ce dépôt inclut une grande diversité de fichiers, allant des guides de médiation aux modèles 3D prêts à être imprimés pour la fabrication des éléments constitutifs des modules. Il est complété par un MédiaWiki.

Les contenus du repositoire et du MédiaWiki sont organisés de manière différente :

- Ce repositoire classe les ressources par _catégorie_ ou par _format de fichier_ (par exemple, tous les fichiers 3D regroupés dans un même dossier).
- Le [MédiaWiki](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/wikis/NANOmus%C3%A9e-:-Documentation), en revanche, structure le contenu _par module_, suivant une approche pédagogique sous forme de tutoriel (par exemple, tous les documents et fichiers nécessaires pour comprendre et assembler un module spécifique).

</br>

[Aller au MédiaWiki](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/wikis/NANOmus%C3%A9e-:-Documentation)

 
Le répositoire est sous licence Creative Commons 1.0 Universelle (CC0 1.0), ce qui permet de copier, modifier, distribuer et utiliser les ressources sans demander l'autorisation de l'auteur. Étant donné que cette licence est la moins restrictive des licences Creative Commons, une clause a été ajoutée pour garantir le caractère non commercial de son utilisation.



## Comment naviguer sur ce repositoire?

6 dossiers principaux se trouvent dans la racine de ce dépôt:

1) [Administration](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/administration)
    - Pilotage + RRHH
    - Conventions artistes et assurances
    - Design: offre d'accompagnement et budget
2) [Communication](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/communication)
    - Charte graphique et Logos (NANOmusée, La Rochelle Université et ses laboratoires, et d'autres structures et institutions impliquées)
    - Images: des modules et une sélection de photos des lieux visités
3) [Démarche Arts-Science](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/demarche%20arts-science)
    - Démarche Arts+Science des modules (travail collaboratif entre chercheurs et artistes)
    - Portrait des artistes (présentation courte + photos)
4) [Fabrication](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/fabrication)
    - Conception et design des modules, par l'atelier de design _Felix Associés_.
    - Guides de fabrication : assemblage, pièces 3D à imprimer, fichiers vecteurs pour le découpage de planches en bois, guide de connexion électrique et électronique.
    - Fichiers multimédia : vidéo, son, code des microcontroleurs, dans le cas de modules qui en utilisent.
5) [Médiation](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/mediation)
    - Présentation générale, guides enseignants cycle 3
    - Livrets et guides de médiation en autonomie
    - Bandes dessinées
6) [Repositoire - Assets](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale/repositoire%20-%20assets)
    - Des ressources nécessaires pour le fonctionnement interne de ce repositoire.

<br>

### Concernant les versions 1 et 2 du NanoMusée
Depuis le lancement du projet en avril 2023 (version 1), le NANOmusée a bénéficié d'une importante mise à jour (version 2). Cette évolution concerne principalement sa fabrication, avec une refonte de l’assemblage et la structure des modules existants, ainsi que l’ajout d’un tout nouveau module.  

La plupart des ressources sont valides pour les deux versions (les guides de médiation en autonomie par exemple). Lorsque des différences existent, les ressources ont été organisées dans des dossiers spécifiques, identifiés comme « v1 » ou « v2 ». C’est notamment le cas des fichiers de découpe des planches.  

Si vous prévoyez de construire vos propres modules, nous vous recommandons d’utiliser les ressources de la v2 : ces modules sont plus faciles à fabriquer, plus légers à transporter et plus respectueux de l’environnement.

## Comment contribuer à ce repositoire?

Il existe plusieurs façons de contribuer, selon votre niveau technique en informatique et le type d’accès au repositoire dont vous disposez. Nous explorerons quelques méthodes de contribution.

Il est important de souligner que tout le monde peut librement télécharger les ressources, même sans l’intention de contribuer activement. Pour ce faire, chaque fichier dispose d'un lien de téléchargement (bouton ⬇️, à droite du fichier). Vous pouvez également télécharger l'integralité du répositoire en vous rendant sur la [page racine (celle-ci)](https://gitlab.univ-lr.fr/aramosan/repositoire-nanomusee/-/tree/principale), puis en cliquant sur le bouton <code>Code</code> ou <code>Clone</code>, en haut à droite, puis en sélectionnant <code>Télécharger le code source</code>. Si votre intention est de télécharger seulement un dossier, ce même menu offre l'option <code>Télécharger ce dossier</code> pour le dossier où vous êtes situé couramment.

Bien que cette méthode soit la plus simple, elle demeure unidirectionnelle : elle vous permet de télécharger les fichiers sans établir de lien avec le repositoire, ce qui ne facilite pas la contribution.

### « J'ai une idée, ou j'ai un fichier d'une nouvelle piéce 3D (par exemple) à apporter, mais je ne suis pas trop doué en informatique »

Si vous souhaitez nous soumettre une proposition ou partager une nouvelle ressource que vous avez créée, mais que vous ne maîtrisez pas encore le système de gestion numérique sur lequel repose le répositoire (GitLab), le plus simple est de nous contacter directement pour nous en expliquer les détails :

> Contact: nanomusee@univ-lr.fr

Cette option constitue une première étape pour collaborer avec le NANOmusée. Toutefois, elle ne permet pas une grande autonomie, car chaque petite modification nécessite une interaction forte avec notre équipe. La collaboration reste ainsi dépendante de notre disponibilité. Cela dit, nous vous encourageons vivement à nous écrire pour partager vos idées et contributions !

### « J'ai des connaissances techniques en informatique et/ou je suis prêt à m'engager activement dans la collaboration »


Si vous souhaitez vous engager plus activement, il sera nécessaire de vous familiariser avec le fonctionnement du repositoire. Cette approche vous permettra de travailler de manière autonome tout en utilisant les outils de gestion spécialisés lors de la collaboration.


#### Fonctionnement technique du repositoire

Le repositoire repose sur la plateforme GitLab, qui, dans notre cas, est associée à La Rochelle Université. Cela signifie qu’un compte universitaire est nécessaire pour un accès complet. Cependant, ce n’est pas une contrainte majeure, car des tiers peuvent y contribuer de différentes manières.

Nous recommandons de commencer par comprendre le système GIT et son intégration dans la plateforme GitLab. GIT est un système informatique de gestion de versions (versioning) et de travail collaboratif. Il permet de sauvegarder les états successifs d’un projet à différents jalons de son développement et facilite le travail en parallèle entre plusieurs collaborateurs, notamment sur les mêmes fichiers.

Bien que le système GIT puisse sembler complexe, une connaissance des opérations de base ( <code>commit</code>, <code>pull</code>, <code>push</code>, <code>merge</code>, <code>branch</code> et <code>fork</code> ) suffit pour commencer à collaborer efficacement.

> [Documentation officielle GIT en anglais](https://git-scm.com/doc)

> [Tutoriel complet GIT](https://www.youtube.com/watch?v=rP3T0Ee6pLU&list=PLjwdMgw5TTLXuY5i7RW0QqGdW0NZntqiP)


<br>

#### Origine et portée de GIT

Initialement conçu par Linus Torvalds pour gérer le développement du système d’exploitation Linux, GIT s’est généralisé pour s’appliquer à tout type de projet nécessitant une gestion collaborative de fichiers numériques. Aujourd’hui, des plateformes comme GitLab, GitHub, ou SourceForge s’appuient sur ce système pour proposer par dessus des interfaces amicales et des outils avancés de gestion de projets.

#### GitLab

GitLab simplifie l’interaction avec GIT grâce à son interface web intuitive, tout en offrant des fonctionnalités supplémentaires pour la gestion de projets. Pour participer, la première étape est de créer un compte GitLab (gratuit pour les projets personnels).

> [Créer un GitLab](https://about.gitlab.com/fr-fr/pricing/)

> [Tutoriel GitLab](https://www.youtube.com/watch?v=q5E-scBPYFA&list=PLn6POgpklwWrRoZZXv0xf71mvT4E0QDOF)

<br>

### Commencer à contribuer: «cloner» le repositoire:

Le premier pas est de _cloner_ le repositoire : le téléchargement d'une copie locale sur votre ordinateur qui sera liée automatiquement à sa version en ligne et que vous permettra de utiliser les systèmes de mises à jour des fichiers de GIT.

Deux façons de cloner existent selon votre affiliation (o pas) à La Rochelle Université: <code>branch</code> ou <code>fork</code>.

> Si vous avez un compte mail de La Rochelle Université, nous pouvons vous ajouter à la gestion du repositoire en tant que membre interne. Veuillez contacter le manager du repositoire ([Agustin Ramos Anzorena](mailto:aramosan@univ-lr.fr)) pour qu'il puisse vous faire parvenir une invitation.


1. Créer une <code>branch</code> du repositoire original. Cela crée une copie parallèle, au sein du même repositoire, ou vous pouvez faire des modifications sans toucher les fichiers originaux. Nommez-la adéquatement.
2. Modifier, ajouter, supprimer les fichiers, toujours sur la même <code>branch</code>. Il faudra se familiariser avec les commandes <code>add</code>, <code>commit</code>, <code>push</code>, <code>pull</code>.
3. Une fois satisfait des modifications, il est possible que celles-ci soient integrées à la <code>branch</code> principale afin de que ce fichier fassent partie de ressources primaires, ouvertes pour tout le monde. À ce props, il faut faire un <code>merge request</code>, où le manager de la <code>branch</code> principale revise et valide les modifications sur votre propre <code>branch</code> pour les intégrer par la suite.


> Si vous n'avez pas de compte universitaire, mais un compte personnel GitLab (ou même GIT dans une autre plateforme), les étapes restent les mêmes avec une modification dans l'étape 1 : au lieu de faire un <code>branch</code>, il faudra faire un <code>fork</code>, qui est similaire sauf que la copie se logera à l'exterieur du repositoire du NanoMusée, dans votre propre repositoire.

<br>
<br>

---
---

<br>
<br>

## License


Ce dépôt est dédié au domaine public sous la Déclaration de Domaine Public Creative Commons CC0 1.0 Universelle avec les conditions supplémentaires suivantes :

1. Vous pouvez utiliser le contenu de ce dépôt à des fins non commerciales sans exigence d'attribution.
2. L'utilisation commerciale est interdite sans le consentement écrit préalable de l'auteur.

Pour en savoir plus, se référer au fichier [LICENSE](./LICENSE.txt)


--


This repository is dedicated to the public domain under the Creative Commons CC0 1.0 Universal Public Domain Dedication with the following additional terms:
1. You may use the contents of this repository for non-commercial purposes without the need for attribution.
2. Commercial use is not permitted without prior written consent from the author.

For more details, see the [LICENSE](./LICENSE.txt) file.
